package org.example;

public class Main {
    public static int GiaiThua(Object m) {
        if (testDauVao(m)) {
            int n = (int) m;
            if (n == 0 || n == 1) {
                return 1;
            } else if (n > 12) {
                throw new IllegalArgumentException("Vượt quá giới hạn tính giai thừa");
            } else if (n < 0) {
                throw new IllegalArgumentException("Không thể tính giai thừa của số âm");
            } else {
                return n * GiaiThua(n - 1);
            }
        }else {
            throw new IllegalArgumentException("Đầu vào không hợp lệ, vui lòng nhập một số nguyên dương");
        }
    }
    public static boolean testDauVao(Object n){
        if(!(n instanceof Integer)){
            return false;
        }else{
            return true;
        }
    }
}