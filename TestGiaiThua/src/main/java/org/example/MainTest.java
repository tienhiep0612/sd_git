package org.example;

import org.junit.Test;
import static org.junit.Assert.assertEquals;


public class MainTest {
    @Test
    public void test0() {
        assertEquals(Main.GiaiThua(0),1 );
    }
    @Test
    public void test12(){
        assertEquals(Main.GiaiThua(12),479001600);
    }
    @Test(expected = IllegalArgumentException.class)
    public void testAm() {
            Main.GiaiThua(-1);
    }
    @Test(expected = IllegalArgumentException.class)
    public void testVuotGioiHan(){
            Main.GiaiThua(13);
    }
    @Test(expected = IllegalArgumentException.class)
    public void testKiTu(){
        Main.GiaiThua("c");
    }
    @Test(expected = IllegalArgumentException.class)
    public void testSoThuc(){
        Main.GiaiThua(1.3);
    }
}


